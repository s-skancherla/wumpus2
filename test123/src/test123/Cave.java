package test123;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Cave {
	//Trivia object
	Trivia trivia;
	
	//2D array of rooms
	Room [][] rooms;
	
	//The x and the Y position of the room
	int startX;
	int startY;

	//Get the room number
	int caveNumber;
	
	//Get the connections of the room
	int[][] connections;
	
	//Constructor of the cave starts the cave num and the x and y
	public Cave(int x1, int y1, int newCaveNum){
		
		//New cave num
		caveNumber = newCaveNum;
		
		//Try trivia
		try {
			trivia = new Trivia();
			
		//Catch exception
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//Sets start X and Y
		startX = x1;
		startY = y1;
		
		//Declares amount of rooms
		rooms = new Room[6][5];
		
		//Connections are made
		connections = new int[30][3];
		int num = 0;
		
		//The y is equal to the start y
		int y = startY;
		
		//Room num starts at 1
		int roomNum = 1;
		
		//For each row in cave
		for(int i = 0; i < 6; i++) {
			int x = startX;
			
			//For each column
			for(int j = 0; j < 5; j++) {
				
				//Increment room number
				num++;
				//Declare a new room
				String [] toAdd = trivia.getHint();
				while(toAdd[0] == null) {
					toAdd = trivia.getHint();
				}
				rooms[i][j] = new Room(num,x,y,roomNum, trivia.getHint());
				roomNum ++;
				x+=50;
			}
			y+=50;
		}
		
		//Ties to find cave file
		try {
			this.getConnections("cave" + caveNumber + ".txt");
			
		//Catch the exception
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Set the connections for each room
		this.setConnections();
		//Hard coded stuff MAKE SURE TO CHANGE
	}
	
	//Getters
	public int getCaveNumber(){
		return caveNumber;
	}
	public Trivia getTrivia(){
		return trivia;
	}
	public Room[][] getCave(){
		return rooms;
	}
	public Room getRoom(int num) {
		//For each column
		for(int i = 0; i < 6; i++) {
			//for each row
			for(int j = 0; j < 5; j++) {
				//If the room has the passed room number
				if(rooms[i][j].getNumber() == num) {
					//Return the room
					return rooms[i][j];
				}
			}
		}
		return null;
	}
	public int[][] getConnections(String filename) throws FileNotFoundException
	{
		Scanner file;
		boolean isMac = false;
		if (isMac) {
			String path = "test123/input/";
			//Sets scanner to read through file
			 file = new Scanner(new File(path + filename));
		} else {
			 file = new Scanner(new File("./input/" + filename));
		}

		
		//For the length of the connections
		for(int r = 0; r < connections.length; r++)
		{
			//The line would be the next line in the file
			String line = file.nextLine();
			
			Scanner sc = new Scanner(line);
			int count = 0;
			
			//While the file still has an int
			while(sc.hasNextInt())
			{
				//If not, then go the next row
				if(count == 0)
				{
					sc.nextInt();
				}
				
				//Update the connections
				connections[r][count] = sc.nextInt();
				count++;
			}
			sc.close();
		}
		//close the file
		file.close();
		return connections;
	}
	
	
	//Sets connections of each room
	public void setConnections()
	{
		int i = 0;
		int j = 0;
		
		//Temp array
		int [] temp = new int[3];
		
		//For each row in connections
		for(int r = 0; r < connections.length; r++)
		{
			//For each column in connections
			for(int c = 0; c < connections[r].length; c++)
			{
				//Temp array is equal that row
				temp[c] = connections[r][c];
			}
			
			//If it reaches the end of the column
			if(j == 5) {
				//Increment i for the row of connections
				i++;
				
				//Reset j to 0 (the column value)
				j = 0;
			}
			
			//Set the rooms connections
			rooms[i][j].setConnections(temp);
			j++;
		}
	}
	
	//Prints the connection for the stored 2D array of connections
	public void printConnections()
	{
		
		//For each row
		for(int r = 0; r < connections.length; r++)
		{
			
			//For each column
			for(int c = 0; c < connections[r].length; c++)
			{
				
				//Print the value
				System.out.print(connections[r][c] + " ");
			}
			System.out.println();
		}
	}

	public int[] findIndex(int room) {
		//Stores array for index
		int[] index = new int[2];
		
		//For each column in rooms
		for(int i = 0; i < rooms.length; i++) {
			
			//For each row in rooms
			for(int j = 0; j < rooms[i].length; j++) {
				
				//if the passed room is equal to the rooms num
				if(room == rooms[i][j].getRoomNum()) {
					
					//Set the index value to its cords
					index[0] = i;
					index[1] = j;
				}
			}
		}
		//Return the array of coordinates
		return index;
	}
}