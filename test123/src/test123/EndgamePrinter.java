package test123;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JFrame;

public class EndgamePrinter extends JFrame implements KeyListener{
	
	//Fields
	private int endMethod;
	private Player player;
	private HighScore testScore;
	int yEnd = 100;
	boolean returnToMenu = false;
	public void paint(Graphics g) {
		Dimension d = this.getSize();
		g.setColor(Color.WHITE);
		g.fillRect(0,0,800,480);
		g.setColor(Color.BLACK);
		g.setFont(new Font("Helvetica", Font.BOLD, 30));
		
		int finalScore = player.getScore();
		if(endMethod == 1) {
			drawCenteredString("YOU WERE EATEN BY THE WUMPUS!", d.width, yEnd, g);
			
		}
		if(endMethod == 2) {
			drawCenteredString("YOU FELL TO YOUR DEATH!", d.width, yEnd, g);
			
		}
		if(endMethod == 3) {
			drawCenteredString("YOU KILLED THE WUMPUS", d.width, yEnd, g);
			
		}
		if(endMethod == 4) {
			drawCenteredString("YOU RAN OUT OF ARROWS", d.width, yEnd, g);
			yEnd+= 30;
			g.setFont(new Font("Helvetica", Font.BOLD, 15));
			drawCenteredString("YOU WERE UNABLE TO PROTECT YOURSELF AND DIED", d.width, yEnd, g);
		}
		if(endMethod == 5) {
			drawCenteredString("YOU RAN OUT OF GOLD", d.width, yEnd, g);
			yEnd+= 30;
			g.setFont(new Font("Helvetica", Font.BOLD, 15));
			drawCenteredString("YOU WERE UNABLE TO PROTECT YOURSELF AND DIED", d.width, yEnd, g);
		}
		//testScore.newScore(player.getScore(), player.toString(), player.getCaveNumber());
		yEnd+= 30;
		g.setFont(new Font("Helvetica", Font.BOLD, 20));
		drawCenteredString("YOUR FINAL SCORE: " + finalScore, d.width, yEnd, g);
		yEnd+= 200;
		testScore.newScore(finalScore, player.toString(),player.getCaveNumber());
		g.setFont(new Font("Helvetica", Font.ITALIC, 20));
		drawCenteredString("CLICK 'ESCAPE' TO RETURN TO THE MAIN MENU", d.width, yEnd, g);

	}
	public void drawCenteredString(String s, int w, int h, Graphics g) {
		FontMetrics fm = g.getFontMetrics();
		int x = (w - fm.stringWidth(s)) / 2;
		g.drawString(s,x,h);
	}
	
	public EndgamePrinter(int endMethod, Player player) {
		setSize(800, 480);
        setResizable(false);
        setTitle("Main Menu");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBackground(Color.black);
        setLocationRelativeTo(null);
        setVisible(true);
        this.endMethod = endMethod;
        this.player = player;
        addKeyListener(this);
        testScore = new HighScore();
	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			dispose();
			MainMenu mainmenu = new MainMenu();
    	}
		else if(e.getKeyCode() ==  KeyEvent.VK_CAPS_LOCK) {
			HighScorePrinter testPrint = new HighScorePrinter(testScore);
		}
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
