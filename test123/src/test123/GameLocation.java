package test123;
import java.util.ArrayList;
public class GameLocation {

	//Stores the room player is in
	Room playerRoom;
	
	//Cave
	Cave cave;
	
	//Stores sprite locations
	int wumpusRoom;
	int [] batRooms;
	int [] pitRooms;
	
	//2D array of the rooms
	Room [][] rooms;
	
	//Times the wumpus for movement
	int wumpusTimer;
	
	//Constructor takes in player room and the cave itself
	public GameLocation(Room room, Cave cave) {
		playerRoom = room;
		this.cave = cave;
		
		//Sets Wumpus room
		this.wumpusRoom = (int)(Math.random()*29+2);
		
		//Randomly generates pit and bat locations
		this.pitRooms = createPitLocations();
		
		this.batRooms = createBatLocations();
		
		//Gets a 2D array of rooms
		rooms = cave.getCave();
		
		//Sets Wumpus move timer to 0
		wumpusTimer = 0;
	}
	
	//Randomly generates pit locations
	public int [] createPitLocations() {
		
		//Generates 2 random Ints
		int randNum = (int)(Math.random()*29 + 2);
		int randNum2 = (int)(Math.random()*29 + 2);
		
		//While the random number is the same as the Wumpus room
		while(randNum == wumpusRoom || cave.getRoom(randNum).getConnection().length == 1) {
			//Regenerate the number
			randNum = (int)(Math.random()*29 + 2);
		}
		
		//While the second random number is the same as the previous number
		//or the Wumpus room
		while(randNum2 == wumpusRoom || randNum2 == randNum || cave.getRoom(randNum2).getConnection().length == 1 ) {
			//Regenerate the number
			randNum2 = (int)(Math.random()*29 + 2);
		}
		
		//Set the pit locations to the randomly generated rooms
		int[]pitRooms = {randNum, randNum2};
		
		//Return the array of pit locations
		return pitRooms;
	}
	
	//For debugging purposes
	public void printLocations() {
		System.out.println("Wumpus: " + wumpusRoom);
		System.out.println("Player: " + playerRoom);
		System.out.println("Pit 1: " + pitRooms[0]);
		System.out.println("Pit 2: " + pitRooms[1]);
	}
	
	//Randomly generate bat locations
	public int [] createBatLocations() {
		
		//Generates 2 random Ints
		int randNum = (int)(Math.random()*29 + 2);
		int randNum2 = (int)(Math.random()*29 + 2);
		
		//While the random number is the same as the Wumpus room or a pit room
		while(randNum == wumpusRoom  || containsPit(randNum)) {
			//Regenerate the number
			randNum = (int)(Math.random()*29 + 2);
		}
		
		//While the random number is the same as the Wumpus room, a pit room or 
		//the previous randomly generated number
		while(randNum == wumpusRoom || randNum2 == randNum || containsPit(randNum)) {
			//Regenerate the number
			randNum2 = (int)(Math.random()*29 + 2);
		}
		
		//set the bat rooms to the randomly generated rooms
		int[]batRooms = {randNum, randNum2};
		
		//return the array of bat locations
		return batRooms;
	}
	
	//updates all locations for game objects
	public int updateLocations(int num) {
		
		//updates the Player room to the num parameter
		updatePlayerRoom(num);
		
		//if the wumpus timer hits 5
		//if(wumpusTimer == 5) {
			//Wumpus moves
			//moveWumpus();
			//Wumpus timer resets
			//wumpusTimer = 0;
		//}
		//Increment wumpus timer
		//wumpusTimer ++;
		
		//if there is a wumpus encounter, return 1 to graphical interface
		if(wumpusEncounter()){
			return 1;
		}
		
		//if there is a pit encounter, return 2 to graphical interface
		else if(pitEncounter()){
			return 2;
		}
		
		//if there is a bat encounter, return 3 to graphical interface
		else if(batEncounter()) {
			return 3;
		}
		
		//otherwise return 0
		return 0;
	}
	
	//Update the player room
	public void updatePlayerRoom(int num){
		
		//Takes the room number parameter and get the Room object of the num
		playerRoom = cave.getRoom(num);
	}
	
	//Update the Wumpus Room
	public void updateWumpusRoom(int num) {
		
		//Takes a number parameter an sets the Wumpus room to that num
		wumpusRoom = num;
	}
	
	//Gets the player room and returns the Room object
	public Room getPlayerRoom() {
		return playerRoom;
	}
	
	//If the room num contains bats
	public boolean containsBat(int roomNum) {
		
		//For all the possible bat locations
		for(int i = 0; i < batRooms.length; i++) 
		{
			//if the location is equal to the passed room number
			if(batRooms[i] == roomNum)
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean containsPit(int roomNum) {
		
		//For all the possible pit locations.
		for(int i = 0; i < pitRooms.length; i++)
		{
			//If the location is equal to the passed room number
			if(pitRooms[i] == roomNum)
			{
				return true;
			}
		}
		
		return false;
	}
	
	//Returns true if the player is new Wumpus, false otherwise
	public boolean nearWumpus() {
		
		//Connections = the rooms connected to player
		int[]connections = playerRoom.getConnection();
		
		//for all the connections
		for(int i = 0; i < connections.length; i++) {
			
			//if one of the connections is equal to the Wumpus room return true
			if(connections[i] == wumpusRoom) {
				return true;
			}
		}
		return false;
	}
	
	//Returns if player is near pit
	public boolean nearPit() {
			
			//Gets the connections of player
			int[]connections = playerRoom.getConnection();
			
			//For each connected room in connections
			for(int i = 0; i < connections.length; i++) {
				
				//for each pit room
				for(int j = 0; j < pitRooms.length; j++) {
					
					//if the connections dont equal 0
					if(connections[i] != 0) {
						
						//And if the connected room to player is equal to a pit room
						if(connections[i] == pitRooms[j]) {
							return true;
						}
					}
				}
			}
			return false;
		}
		
	//Returns if player is near a bat
	public boolean nearBat() {
			
			//Gets the connections of player
			int[]connections = playerRoom.getConnection();
			
			//For each connected room in connections
			for(int i = 0; i < connections.length; i++) {
				
				//for each bat room
				for(int j = 0; j < batRooms.length; j++) {
					
					//if the connections dont equal 0
					if(connections[i] != 0) {
						
						//And if the connected room to player is equal to a bat room
						if(connections[i] == batRooms[j]) {
							return true;
						}
					}
				}
			}
			return false;
		}
		
	//Finds if the wumpus is hit
	public boolean nearWumpusShoot(int direction) {
			
		//First checks if player is near Wumpus
		if(nearWumpus()) {
			
			//If the direction is 1 (up)
			if(direction == 1) {
				
				//Checks if the possible connections above the player contians Wumpus
				if(playerRoom.getRoomNum() - 5 == wumpusRoom || playerRoom.getRoomNum() + 25 == wumpusRoom) {
					return true;
				}
			}
			
			//If the direction is 2 (left)
			else if(direction == 2) {
				
				//Checks if the possible connections left of the player contians Wumpus
				if(playerRoom.getRoomNum() - 1 == wumpusRoom || playerRoom.getRoomNum() + 4 == wumpusRoom) {
					return true;
				}
			}
			
			//If the direction is 3 (right)
			else if(direction == 3) {
				
				//Checks if the possible connections right of the player contians Wumpus
				if(playerRoom.getRoomNum() + 1 == wumpusRoom || playerRoom.getRoomNum() - 4 == wumpusRoom) {
					return true;
				}
			}
			
			//If the direction is 4 (down)
			else if(direction == 4) {
				
				//Checks if the possible connections down of the player contains Wumpus
				if(playerRoom.getRoomNum() + 5 == wumpusRoom || playerRoom.getRoomNum() - 25 == wumpusRoom) {
					return true;
				}
			}
		}
		
		//Wumpus is not shot
		return false;
	}
	
	//If the player is same room as wumpus return true
	public boolean wumpusEncounter() {
		
		//if the player is in the same room
		if(wumpusRoom == playerRoom.getRoomNum())
		{
			return true;
		}
		return false;
	}

	public boolean batEncounter() {
		for(int i = 0; i < batRooms.length; i++) {
			if(batRooms[i] == playerRoom.getRoomNum())
			{
				movePlayerRand();
				batRooms[i] = moveBatRand();
				return true;
			}
		}
		return false;
	}
	
	//If the player encounters a pit
	public boolean pitEncounter() {
		
		//One pit equals false
		boolean onePit = false;
		
		//For all the possible pits
		for(int i = 0; i < pitRooms.length; i++) {
			
			//if the pit room is equal to the player room number
			if(pitRooms[i] == playerRoom.getRoomNum())
			{
				//Update the player room number to room 1
				updatePlayerRoom(1);
				return true;
			}
		}
		return false;
	}
	
	//Moves the wumpus to a nearby room that is connected
	public void moveWumpus()
	{
		
		//Declares an array list of the possible connections
		ArrayList<Integer> connect = new ArrayList<Integer>();
		
		//Room contains index of the wumpus Room
		int [] room  = cave.findIndex(wumpusRoom);
		
		//Gets the connection of the Wumpus room
		int[] connections = rooms[room[0]][room[1]].getConnection();
		
		//For all the connections
		for(int i = 0; i < connections.length; i++) {
			
			//if the connection isnt 0 (null upon creation)
			if(connections[i] != 0) {
				
				//Add it to the array list
				connect.add(connections[i]);
			}
		}
		
		//Generates a random number based of the length of possible connections
		int randRoom= (int) (Math.random()*connect.size());
		
		//Finds the random room that is connected
		randRoom = connect.get(randRoom);
		
		//Time created to control the amount of times the while loop generates random rooms
		int timer = 0;
		
		//While the cave contains pits, bats, the player AND the timer hasn't run out
		while((containsPit(randRoom) || containsBat(randRoom) || randRoom == playerRoom.getRoomNum()) && timer < 16 ) {
			
			//regenerates a random room that is connected to Wumpus
			randRoom = (int) (Math.random()*connect.size());
			randRoom = connect.get(randRoom);
			
			//timer ++
			timer ++;
		}
		
		//if the timer didn't run out
		if(timer < 15) {
			
			//Update the wumpus room number
			this.updateWumpusRoom(randRoom);
		}
		//Wumpus stays in same room because it is surrounded by the player or hazards
	}
	
	//Moves Player to a random location
	public void movePlayerRand()
	{
		
		//Generates a random room
		int randRoom = (int) (Math.random()*30 + 1);
		
		//While the random room contains pits, bats, wumpus or the previous player oom
		while(containsPit(randRoom)|| containsBat(randRoom) || randRoom == wumpusRoom || randRoom == playerRoom.getRoomNum()) {
			//generate another room number
			randRoom = (int) (Math.random()*30 + 1);
		}
		
		//Update the player room to the randomly generated value
		this.updatePlayerRoom(randRoom);
	}
	
	//Moves bats to a random location
	public int moveBatRand()
	{
		
		//Generates a random room
		int randRoom = (int) (Math.random()*29 + 2);
		
		//While the random room contains a pit, the player, a wumpus or bats
		while(containsPit(randRoom) || randRoom == playerRoom.getRoomNum() || randRoom == wumpusRoom || containsBat(randRoom)) {
			//generate another room number
			randRoom = (int) (Math.random()*29 + 2);
			
		}
		
		//Return the randomly generated room
		return randRoom;
	}
	
	//Moves pits to a random location
	public int movePitRand()
	{
		
		//Generates a random room
		int randRoom = (int) (Math.random()*29 + 2);
		
		//While the random room contains a pit, the player, a wumpus or bats
		while(containsPit(randRoom) || randRoom == playerRoom.getRoomNum() || randRoom == wumpusRoom || containsBat(randRoom)) {
			//generate another room number
			randRoom = (int) (Math.random()*29 + 2);
		}
		
		//Return the randomly generated room
		return randRoom;
	}
	
	//Move the Wumpus to a random location
	public void moveWumpusRand()
	{
		//Generate a random room
		int randRoom = (int) (Math.random()*29 + 2);
		
		//If the random room contains pits, bats, or the player
		if(containsPit(randRoom) || containsBat(randRoom) || randRoom == playerRoom.getRoomNum()) {
			
				//Generate a new random room
				randRoom = (int) (Math.random()*29 + 2);
		}
		
		///Update the wumpus room number
		this.updateWumpusRoom(randRoom);
	}
	
	//returns an string as the roomNumber for a hazard
	public String getSecret()
	{
		
		//Generate a random number
		int random = (int)(Math.random() * 4);
		
		//based off the random number, return a secret
		if(random == 0)
		{
			return "There is a bat in room " + batRooms[0];
		}
		else if(random == 1)
		{
			return "There is a bat in room " + batRooms[1];
		}
		else if(random == 2)
		{
			return "There is a pit in room " + pitRooms[0];
		}
		else
		{
			return "The Wumpus is in room " + wumpusRoom;
		}
	}
	
	//To String
	public String toString(){
		return "GameLocations";
	}
	
}
