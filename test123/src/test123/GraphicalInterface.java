package test123;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class GraphicalInterface extends JFrame implements KeyListener{
	int x=90;
	int y=100;
	int xNotification = 400 ;
	int yNotification = 150;
	int currentRoom = 1;
	Cave cave;
	Player player;
	Room rooms[][];
	int surroundingRooms[];
	GameLocation location;
	Trivia trivia;

	int triviaGameType = 0;

	boolean returnToMenu = false;
	boolean fallPit = true;
	boolean addArrow = false;
	
	public void paint(Graphics g) {
		if(player.getNumArrows() == 0) {
			EndgamePrinter test = new EndgamePrinter(4,player);
		}
		try {
			runner(g);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void runner(Graphics g) throws IOException {
		
		
		
		//If player earned an arrow previously, the new count will be printed on the screen
		if(addArrow) {
			player.gainArrow();
		}
		addArrow = false;
		
		//For debugging purposes
		location.printLocations();
		
		
		//Sets background
		g.setColor(Color.WHITE);
		g.fillRect(0,0,800,580);
		int yResult = 100;
		Dimension d = this.getSize();


		//Checks if trivia game should be played and if so, what message should be printed
		g.setColor(Color.BLACK);
		g.setFont(new Font("Helvetica", Font.BOLD, 20));
				

		//Prints options at bottom of screen
		g.drawString("Type '1' to purchase arrows", 90, 450);
		g.drawString("Type '2' to purchase a secret", 90, 480);
		
		//Mode 1 - If the user wants to earn arrows
		if(triviaGameType == 1) {
			TriviaPrinter test;
			try {
					test = new TriviaPrinter(1,3,player);
					if(test.getResult()){
						addArrow = true;
					}
					
			} catch (Exception e) {				
				e.printStackTrace();
			}
			repaint();
		}
		
		//Mode 2 - If the user is attempting to earn a secret
		if(triviaGameType == 2) {
			TriviaPrinter test;
			try {
					test = new TriviaPrinter(2,3,player);
					test.setSecret(location.getSecret());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			repaint();
		}
		triviaGameType = 0;
		
		
		//Creates background for map
		g.setColor(Color.BLACK);
		g.fillRect(90, 100, 250, 300);
		int oldRoom = -1;
		int xOld = -1;
		int yOld = -1;
		
		
		//Updates the user location if they move
		int val = location.updateLocations(currentRoom);
		if(val == 2) {
			oldRoom = currentRoom;
		}
		currentRoom = location.getPlayerRoom().getRoomNum();
		
		//The user's current room is colored white
		int y1 = 100;
		for(int i = 0; i < 6; i++) {
			int x1 = 90;
			for(int j = 0; j < 5; j++) {
				g.setColor(Color.WHITE);
				g.drawRect(x1,y1, 50, 50);
				x1+=50;
			}
			y1+=50;
		}
		
		//Prints player name at the top of the screen
		g.setColor(Color.BLACK);
		g.setFont(new Font("Helvetica", Font.BOLD, 20));
		g.drawString(player.toString(), 90, 90);
			
		//Updates player location if needed
		for(int i = 0; i < 6; i++) {
			for(int j = 0; j < 5; j++) {
			    if(rooms[i][j].getNumber() == currentRoom) {
			          x = rooms[i][j].getX();
			          y = rooms[i][j].getY();
			    }
				else if(rooms[i][j].getNumber() == oldRoom) {
					xOld = rooms[i][j].getX();
					yOld = rooms[i][j].getY();
				}		
			}
		}
		
		//Updates and displays console
		console(g, xOld, yOld, val);
		
		//Sets user current room to white
		g.setColor(Color.WHITE);
		g.fillRect(x, y, 50, 50);

		//Gets connections of current room
		int[]index = cave.findIndex(currentRoom);
		surroundingRooms = rooms[index[0]] [index[1]].getConnection();  
		
		
		//Changes color of rooms that player can move into to gray
		for(int i = 0; i < surroundingRooms.length; i++) {
			for(int r = 0; r < rooms.length; r++) {
				for(int c = 0; c < rooms[r].length; c++) {
					if(surroundingRooms[i] != 0) {
						if(surroundingRooms[i] == rooms[r][c].getRoomNum()) {
							g.setColor(Color.DARK_GRAY);
							g.fillRect(rooms[r][c].getX(), rooms[r][c].getY(), 50, 50);
							g.setColor(Color.WHITE);
							g.drawRect(rooms[r][c].getX(), rooms[r][c].getY(), 50, 50);
							
						}
					}
				}
			}
		}
	}
	
	
	//Handles console (warnings and room location) and hazards
	public void console(Graphics g, int xOld, int yOld, int val) throws IOException {
		
				//Prints any warnings that may be applicable
				g.setColor(Color.WHITE);
				g.fillRect(xNotification, yNotification-40, 400, 600);
				
				//Prints header
				g.setColor(Color.BLACK);
				g.setFont(new Font("Helvetica", Font.BOLD, 20));
				g.drawString("WARNINGS:", xNotification, yNotification-30);
				g.setFont(new Font("Helvetica", Font.BOLD, 15));
				
				//Prints any applicable warnings
				
				//Warning if wumpus is nearby
				if(location.nearWumpus()) {
					g.drawString("I SMELL A WUMPUS!", xNotification, yNotification);
					yNotification+=30;
					
				}
				
				//Warning if bat is nearby
				if(location.nearBat()) {
					g.drawString("BATS NEARBY!", xNotification, yNotification);
					yNotification+=30;
					
				}
				
				//Warning if pit is nearby 
				if(location.nearPit()) {
					g.drawString("I FEEL A DRAFT!", xNotification, yNotification);
					yNotification+=30;
				}
				
				//Prints 'nothing to be displayed' if there are no current warning
				yNotification-=30;
				if(!location.nearBat() && !location.nearPit() && !location.nearWumpus()) {
					yNotification+=30;
					g.setFont(new Font("Helvetica", Font.ITALIC, 15));
					g.drawString("There are no warnings at this time", xNotification, yNotification);
				}
				
				//Prints the room that the user is currently in
				yNotification += 40;
				g.setFont(new Font("Helvetica", Font.BOLD, 20));
				g.drawString("ROOM INFORMATION:", xNotification, yNotification);
				g.setFont(new Font("Helvetica", Font.BOLD, 15));
				yNotification += 30;
				g.drawString("Current Room Number: " + location.getPlayerRoom().getNumber(), xNotification, yNotification);
				
				//Notifies the user about what room they are in
				yNotification += 30;
				g.setFont(new Font("Helvetica", Font.BOLD, 15));
				
				//Displays any relevant information about the room the user is in
				
				//Prints that the wumpus is still close by
				if(val == 1) {
					g.drawString("THE WUMPUS IS STILL NEARBY", xNotification, yNotification);
					g.setColor(Color.RED);
					TriviaPrinter test = new TriviaPrinter(4,5,player);
					location.moveWumpusRand();
					repaint();
				}
				
				//If the user encounters a pit and survives, they are moved back to room 1
				else if(val == 2) {

                    g.setColor(Color.BLUE);
                    g.fillRect(xOld, yOld, 50, 50);
                    g.setColor(Color.WHITE);
                    g.drawRect(xOld,yOld, 50, 50);
                    TriviaPrinter test = new TriviaPrinter(3,3,player);
                    g.setColor(Color.BLACK);
                    g.drawString("YOU WERE MOVED BACK TO ROOM 1", xNotification, yNotification);
                    location.updateLocations(1);
                    g.setColor(Color.BLACK);
                    g.fillRect(xOld, yOld, 50, 50);
                    g.setColor(Color.WHITE);
                    g.drawRect(xOld, yOld, 50,50);
                    g.fillRect(x, y, 50, 50);
                    fallPit = true;
                    repaint();
				}
				
				//Prints if the user encounters a bat
				else if(val == 3) {
					
					//Moves the player to a random location
					location.updateLocations(currentRoom);
					g.drawString("YOU ENTERED A ROOM WITH A BAT!", xNotification, yNotification);
					yNotification += 20;
					g.drawString("THE BAT TOOK YOU TO ROOM " + location.getPlayerRoom().getNumber(), xNotification, yNotification);
					g.setColor(Color.WHITE);
					
				}
				
				//If there is nothing special about the room, the following is printed
				else {
					g.setFont(new Font("Helvetica", Font.ITALIC, 15));
					g.drawString("Normal Room", xNotification, yNotification);
				}
				yNotification+=40;
				
				
				
				//Gives a hint to player when going through tunnel
				
				//Prints header
				g.setColor(Color.BLACK);
				g.setFont(new Font("Helvetica", Font.BOLD, 20));
				g.drawString("HINT:", xNotification, yNotification);
				
				
				//Prints if there are no hints
				if(player.getRoom() == 1) {
					
					yNotification += 30;
					g.setFont(new Font("Helvetica", Font.ITALIC, 15));
					g.drawString("There are no hints available at this time", xNotification, yNotification);
				}
				
				//Prints and formats hint
				else {
					
					yNotification += 30;
					g.setFont(new Font("Helvetica", Font.BOLD, 15));
					
					//Prints question
					String[] hint = location.getPlayerRoom().getTriviaAnswer();
					String toAsk = hint[0];
					String savedQ = toAsk;
					boolean printQuestion = true;
					int onIndex = 0;
					String toPrint = "";

					while(printQuestion) {
						int lengthRem = toAsk.length();
						if(lengthRem > 20) {
							toPrint = toAsk.substring(0,20);
							toAsk = toAsk.substring(20);
							onIndex += 20;
							while(onIndex < savedQ.length() && toAsk.charAt(0) != ' ') {
								toPrint += toAsk.charAt(0);
					
								toAsk = toAsk.substring(1);
								onIndex++;
							}
							if(toPrint.charAt(0) == ' ') {
								toPrint = toPrint.substring(1);
							}
							g.drawString(toPrint,xNotification,yNotification);
							yNotification+=20;
						}
						else {
							g.drawString(toAsk,xNotification,yNotification);
							printQuestion = false;
						}
					}
					yNotification+=40;
					
					//Prints answer
					g.setFont(new Font("Helvetica", Font.BOLD, 15));
					g.drawString(hint[1], xNotification, yNotification);
				}
				yNotification+=40;
				
				
				//Shows how many arrows the user has
				g.setColor(Color.BLACK);
				g.setFont(new Font("Helvetica", Font.BOLD, 20));
				g.drawString("ARROW COUNT:", xNotification, yNotification);
				String arrowCount = Integer.toString(player.getNumArrows());
				g.drawString(arrowCount, xNotification + 170, yNotification);
				yNotification+=40;
				
				//Shows how much gold the user has
				g.setColor(Color.BLACK);
				g.setFont(new Font("Helvetica", Font.BOLD, 20));
				g.drawString("GOLD:", xNotification, yNotification);
				String goldCount = Integer.toString(player.getGold());
				g.drawString(goldCount, xNotification + 70, yNotification);
				
				
				yNotification = 150;

	}

	//A method used to print a string in the middle of the screen
	public void drawCenteredString(String s, int w, int h, Graphics g) {
		FontMetrics fm = g.getFontMetrics();
		int x = (w - fm.stringWidth(s)) / 2;
		g.drawString(s,x,h);
	}
		
	
	public void keyPressed(KeyEvent e) {}
	
	//Listens for key presses
    public void keyReleased(KeyEvent e) { 
    	//User moves right if possible
    	if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
    		int lastRoom = currentRoom;
    		for(int i = 0; i < surroundingRooms.length; i++) {
    			if(!(currentRoom % 5==0)) {
    				if(currentRoom + 1 == surroundingRooms[i]) {
    					currentRoom++;
    				}
    			}
    			else {
    				if(currentRoom - 4 == surroundingRooms[i]) {
    					currentRoom = currentRoom - 4;
    				}
    			}
    		}
    		if(currentRoom != lastRoom) {
    			player.moveForward();
        		repaint();
    		}
    	}
    		
    	//User moves left is possible
    	else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
    		//System.out.println("Left");
    		int lastRoom = currentRoom;
    		for(int i = 0; i < surroundingRooms.length; i++) {
    			if(!(currentRoom % 5==1)) {
    				if(currentRoom - 1 == surroundingRooms[i]) {
    	    			currentRoom--;
    				}
    			}
    			else {
    				if(currentRoom + 4 == surroundingRooms[i]) {
    					currentRoom = currentRoom + 4;
    				}
    			}
    		}
    		if(currentRoom != lastRoom) {
    			player.moveForward();
        		repaint();
    		}
    	}
    	
    	//User moves down if possible
    	else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
    		int lastRoom = currentRoom;
    		for(int i = 0; i < surroundingRooms.length; i++) {
    			if(currentRoom < 26) {
    				if(currentRoom + 5 == surroundingRooms[i]) {
    	    			currentRoom+=5;
    				}
    			}
    			else {
    				if(currentRoom - 25 == surroundingRooms[i]) {
    		   			currentRoom = currentRoom - 25;
    				}
    			}
    		}
    		if(currentRoom != lastRoom) {
    			player.moveForward();
        		repaint();
    		}
    	}
    	
    	//User moves up if possible 
    	else if (e.getKeyCode() == KeyEvent.VK_UP) {
    		int lastRoom = currentRoom;
    		for(int i = 0; i < surroundingRooms.length; i++) {
    			if(currentRoom > 5) {
    				if(currentRoom - 5 == surroundingRooms[i]) {
    					currentRoom-=5;
    				}
    			}
    			else {
    				if(currentRoom + 25 == surroundingRooms[i]) {
    	    			currentRoom = currentRoom + 25;
    				}
    			}
    		}
    		if(currentRoom != lastRoom) {
    			player.moveForward();
        		repaint();
    		}
    		
    	}

    	//Shoots arrow to the room directly above
    	else if(e.getKeyCode() == KeyEvent.VK_W){
    		if(player.shootArrow(1,location)) {
    			EndgamePrinter test = new EndgamePrinter(3,player);
    		}
    		else {
    			JOptionPane.showMessageDialog(null, "That arrow missed the Wumpus!");
    		}
    		repaint();
    	}
    	
    	//Shoots arrow to the room directly to the left
    	else if(e.getKeyCode() == KeyEvent.VK_A){
    		if(player.shootArrow(2,location))
    		{
    			EndgamePrinter test = new EndgamePrinter(3,player);
    		}
    		else {
    			JOptionPane.showMessageDialog(null, "That arrow missed the Wumpus!");
    		}
    		repaint();
    	}
    	
    	//Shoots arrow to the room directly to the left
    	else if(e.getKeyCode() == KeyEvent.VK_D){
    		if(player.shootArrow(3,location)) {
    			EndgamePrinter test = new EndgamePrinter(3,player);    		
    		}
    		else {
    			JOptionPane.showMessageDialog(null, "That arrow missed the Wumpus!");
    		}
    		repaint();
    	}
    	
    	//Shoots arrow to the room directly to the left
    	else if(e.getKeyCode() == KeyEvent.VK_S){
    		if(player.shootArrow(4,location)) {
    			EndgamePrinter test = new EndgamePrinter(3,player);
    			
    		}
    		else {
    			JOptionPane.showMessageDialog(null, "That arrow missed the Wumpus!");
    		}
    		repaint();
    	}
    	
    	//User can purchase arrow
    	else if(e.getKeyCode() == KeyEvent.VK_1){
    		if(player.getGold() < 3) {		
				JOptionPane.showMessageDialog(null, "You do not have enough gold!");
			}
    		else {
    			triviaGameType = 1;
    		}
    		
    		repaint();
    	}
    	
    	//User can purchase secret
    	else if(e.getKeyCode() == KeyEvent.VK_2){
    		if(player.getGold() < 3) {		
				JOptionPane.showMessageDialog(null, "You do not have enough gold!");
			}
    		else {
    			triviaGameType = 2;
    		}
    		repaint();
    	}
    	
    	player.setRoom(currentRoom);
    	
    	
    }
    
    public void keyTyped(KeyEvent e) {}

    //Constructor
	public GraphicalInterface(String name, Cave cave) throws IOException {
		this.cave = cave;
		this.player = new Player(name, cave.getCaveNumber());
		trivia = new Trivia();
		setSize(800, 580);
        setResizable(false);
        setTitle("Faze Clan Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBackground(Color.WHITE);
        setLocationRelativeTo(null);
        setVisible(true);
        addKeyListener(this);
		rooms = cave.getCave();
        location = new GameLocation(rooms[0][0],cave);
      
	}
	
	
}
