package test123;

import java.util.*;
import java.io.*;
public class HighScore {
	public static final int NUM_HIGH_SCORES= 10;
	public static final String HIGH_SCORE_FILE = "highScore.dat";
	private ArrayList<Score> highScores;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private ScoreComparator compare;
	
	//HighScore constructor
	public HighScore()
	{
		input = null;
		output = null;
		compare = new ScoreComparator(); 
		highScores = new ArrayList<Score>();
	}
	
	//sorts the highScores using the ScoreComparator class
	public void sort()
	{
		Collections.sort(highScores, compare);
	}
	
	/*returns an arrayList of highScores by loading the score file, sorting the highScores,
	and removing any extra highScores*/
	public ArrayList<Score> getHighScores()
	{
		loadScoreFile();
		sort();
		for(int i = highScores.size() - 1; i > 9; i--)
		{
			highScores.remove(i);
		}
		return highScores;
	}
	
	/*loads the highScore file, adds a new highScore, and updates the highScore file
	with the new highScore*/
	public void newScore(int score, String playerName, int caveNum)
	{
		loadScoreFile();
		highScores.add(new Score(score, playerName, caveNum));
		updateHighScore();
	}
	
	@SuppressWarnings("unchecked")
	//loads the highScore file using an input stream
	public void loadScoreFile()
	{
		try {
			input = new ObjectInputStream(new FileInputStream(HIGH_SCORE_FILE));
			highScores = (ArrayList<Score>) input.readObject();
		}
		catch (FileNotFoundException w)
		{
			System.out.println("File not found");
		}
		catch (IOException x)
		{
			System.out.println("IO error");
		}
		catch (ClassNotFoundException y)
		{
			System.out.println("CNF error");
		}
		finally {
			try {
					if(output != null)
					{
						output.flush();
						output.close();
					}
			}
			catch (IOException z)
			{
				System.out.println("IO error");
			}
		}
	}
	
	//writes the new highScore into the file using an output stream
	public void updateHighScore()
	{
		try {
			output = new ObjectOutputStream(new FileOutputStream(HIGH_SCORE_FILE));
			output.writeObject(highScores);
		}
		catch (FileNotFoundException w)	
		{
			System.out.println("File not found");
		}
		catch (IOException x)
		{
			System.out.println("IO error");
		}
		finally {
			try {
					if(output != null)
					{
						output.flush();
						output.close();
					}
			}
			catch (IOException z)
			{
				System.out.println("IO error");
			}
		}
	}
	
	//returns a String of highScores all concatenated together
	public String displayHighScores()
	{
		highScores = this.getHighScores();
		String total = "";
		for(int i = 0; i < highScores.size(); i++) {
			total += highScores.get(i).toString() + "\n";
		}
		return total;
	}
}

