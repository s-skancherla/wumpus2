package test123;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;

public class HighScorePrinter extends JFrame implements KeyListener{
	int yScore = 100;
	HighScore testScore;
	
	//Paint function creates a screen with highScores
	public void paint(Graphics g) {
		
		//Sets screen
		Dimension d = this.getSize();
		g.setColor(Color.WHITE);
		g.fillRect(0,0,800,480);
		g.setColor(Color.BLACK);
		g.setFont(new Font("Helvetica", Font.BOLD, 40));
		
		//Title
		drawCenteredString("HIGH SCORES", d.width, yScore, g);
		yScore += 40;
		g.setFont(new Font("Helvetica", Font.BOLD, 20));
		
		//Gets list of high scores
		ArrayList<Score> toPrint = testScore.getHighScores();
		
		//Prints high scores
		for(int i = 1; i <= toPrint.size();i++) {
			drawCenteredString(i + ") " + toPrint.get(i-1).toString(),d.width,yScore,g);
			yScore+=30;
		}
		
		//Message allowing users to return to main menu
		g.setFont(new Font("Helvetica", Font.ITALIC, 20));
		drawCenteredString("PRESS 'ESCAPE' TO RETURN TO MENU", d.width, yScore, g);
		yScore = 100;
	}
	
	//Draws strings in the center of the screen
	public void drawCenteredString(String s, int w, int h, Graphics g) {
		FontMetrics fm = g.getFontMetrics();
		int x = (w - fm.stringWidth(s)) / 2;
		g.drawString(s,x,h);
	}
	
	//HighScorePrinter constructor
	public HighScorePrinter(HighScore testScore) {
		setSize(800, 480);
        setResizable(false);
        setTitle("Main Menu");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBackground(Color.black);
        setLocationRelativeTo(null);
        setVisible(true);
        this.testScore = testScore;
        addKeyListener(this);
        
	}

	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void keyPressed(KeyEvent e) {}

	//Checks if the keyReleased has had the event called, and initiates an action is pressed
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
			dispose();
		}
	}

}
