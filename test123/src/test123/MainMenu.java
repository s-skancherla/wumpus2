package test123;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

import javax.swing.JFrame;

public class MainMenu extends JFrame implements KeyListener {

	String name = "Enter name";
	HighScore list;
	int onCaveNum = 1;
	Boolean startGame = false;
	
	//Paint function creates a screen with the MainMenu object
	public void paint(Graphics g) {
		Dimension d = this.getSize();
		g.clearRect(0,0,800,480);
		g.setColor(Color.WHITE);
		
		g.fillRect(0,0,800,480);
		g.setColor(Color.BLACK);
		g.setFont(new Font("Helvetica", Font.BOLD, 40));
		drawCenteredString("HUNT THE WUMPUS", d.width, 100, g);

		g.setFont(new Font("Helvetica", Font.ITALIC, 20));
		drawCenteredString("SELECT CAVE", d.width, 160, g);
		
		
		int [] xCoordinates1 = {300,315,315};
		int [] yCoordinates1 = {200, 215, 185};
		g.fillPolygon(xCoordinates1, yCoordinates1, 3);
		g.setFont(new Font("Helvetica", Font.BOLD, 30));
		drawCenteredString("CAVE " + onCaveNum, d.width, 210, g);
		
		int [] xCoordinates2 = {500,485,485};
		int [] yCoordinates2 = {200, 215, 185};
		g.fillPolygon(xCoordinates2, yCoordinates2, 3);

		
		g.setFont(new Font("Helvetica", Font.ITALIC, 20));
		drawCenteredString("ENTER PLAYER NAME", d.width, 270, g);
		g.setFont(new Font("Helvetica", Font.BOLD, 30));
		
		drawCenteredString(name,d.width,310,g);
		
		g.setFont(new Font("Helvetica", Font.ITALIC, 20));
		drawCenteredString("PRESS 'ENTER' TO START GAME", d.width, 380, g);
		drawCenteredString("PRESS 'CAPS LOCK' TO VIEW HIGH SCORES", d.width, 410, g);
		
	}
	
	//Draws Strings on the Screen
	public void drawCenteredString(String s, int w, int h, Graphics g) {
		FontMetrics fm = g.getFontMetrics();
		int x = (w - fm.stringWidth(s)) / 2;
		g.drawString(s,x,h);
	}
	
	//MainMenu constructor
	public MainMenu() {
		setSize(800, 480);
        setResizable(false);
        setTitle("Main Menu");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBackground(Color.black);
        setLocationRelativeTo(null);
        setVisible(true);
        addKeyListener(this);
	}

	@Override
	//Checks for key presses and initiates different actions when different keys are pressed
	public void keyPressed(KeyEvent e) {
		
		
		if(e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
			name = name.substring(0,name.length()-1);
		}
		else if(e.getKeyCode() ==  KeyEvent.VK_CAPS_LOCK) {
			HighScore testScore = new HighScore();
			HighScorePrinter testPrint = new HighScorePrinter(testScore);
		}
		else if(e.getKeyCode() ==  KeyEvent.VK_ENTER) {
			startGame = true;
			Cave cave = new Cave(90,100,onCaveNum);
			try {
				GraphicalInterface game = new GraphicalInterface(name, cave);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		else if(e.getKeyCode() ==  KeyEvent.VK_LEFT) {
			if(onCaveNum > 1) {
				onCaveNum--;
			}
		}
		else if(e.getKeyCode() ==  KeyEvent.VK_RIGHT) {
			if(onCaveNum < 5) {
				onCaveNum++;
			}
		}
		else {
			if((e.getKeyCode() >= 65 && e.getKeyCode() <= 90) || e.getKeyCode() == KeyEvent.VK_SPACE){
				name += e.getKeyChar();
			}
			
		}
		repaint();
	}


	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
