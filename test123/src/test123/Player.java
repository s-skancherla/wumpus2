package test123;

public class Player {
	
	//Player room
	int currentRoom;
	
	//Player inventory
	private int arrowCount;
	private int gold;
	private int caveNumber;
	//Player stats
	private int turnsTaken;
	private boolean wumpusKilled;
	
	//Player info
	private String playerName;
	int wumpusDirection;
	
	//Player constructor
	public Player(String playerName, int caveNum)
	{
		this.playerName = playerName;
		this.caveNumber = caveNum;
		arrowCount = 3;
		gold = 0;
		turnsTaken = 0;
		wumpusKilled = false;
		currentRoom = 1;
	}
	
	//Modifiers
	public void setRoom(int num) {
		currentRoom = num;
	}
	public void changeGold(int amount) {
		gold += amount;
	}
	public void gainArrow() {
		arrowCount++;
	}
	public void changePlayerName(String newName) {
		playerName = newName;
	}
	public void changeTurnsTaken(int amount) {
		turnsTaken += amount;
	}
	
	//Getters
	public int getCaveNumber(){
		return caveNumber;
	}
	public int getRoom() {
		return currentRoom;
	}
	public String toString(){
		return playerName;
	}
	public int getTurns() {
		return turnsTaken;
	}
	public int getGold() {
		return gold;
	}
	public int getNumArrows() {
		return arrowCount;
	}

	/*
	 Score calculation uses the formula of:
	 * 100 points - N + G + (5*A) + W
	 * Where: 
	 * N = # of turns
	 * G = # of gold left
	 * A = # of arrows left
	 * W = 50 if wumpus is killed, 0 otherwise
	 */
	public int getScore(){
		
		//Score formula
		int score = 100 - turnsTaken + gold + (5 * arrowCount);
		//if the Wumpus is killed add 50 to score
		if(wumpusKilled)
		{
			score += 50;
		}
		if(score < 0)
		{
			score = 0;
		}
		//otherwise just return the score
		return score;
	}

	
	//+ 1 gold, turns updated + 1
	public void moveForward() 
	{
		if(gold <= 99)
		{
			changeGold(1);
		}
		changeTurnsTaken(1);
	}
	
	//If the user wants to buy an arrow; if they cant return false
	public boolean buyArrow()
	{
		
		//if they have more than or equal to 3 gold
		if(gold >= 3)
		{
			
			//Arrow count is updated, gold is subtracted
			arrowCount ++;
			gold -= 3;
			return true;
		}
		
		//If they dont have enough gold,return false
		else
		{
			return false;
		}
	}
	
	//Shoot arrow and returns true if it hits
	public boolean shootArrow(int direction, GameLocation location)
	{
		//if the user does not have enough arrows
		if(arrowCount <= 0)
		{
			
			EndgamePrinter test = new EndgamePrinter(4, this);
			//display an error message
			
			System.out.println("Not enough arrows!");
			return false;
		}
		
		arrowCount = arrowCount - 1;
		
		//Calls the wumpus Shoot method from location to
		//determine if the wumpus is hit in the passed direction
		if(location.nearWumpusShoot(direction))
		{
			System.out.println("Wumpus Killed!");
			wumpusKilled = true;
			return true;

		}
		
		//Move the wumpus to a random location
		location.moveWumpusRand();
		return false;
	}
}
