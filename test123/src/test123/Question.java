package test123;

public class Question {

	private String question;
    private String[]options;
    private String answer;
    private String optionsString;
    
    //Question constructor
    public Question(String question, String[] options, String answer){

        this.question = question;
        this.options = options;
        this.answer = answer;
        this.optionsString = "";

    }
    
    //returns the question
    public String getQuestion(){
        return question;
    }
    
    //returns a string array of the possible answers
    public String[] getOptions(){
        return options;
    }
    
    //returns the letter choice of the answer
    public String getAnswer(){
        return answer;
    }
    
    //returns a concatenated String of the possible answers
    public String getOptionsString() {
        for(int i = 0; i < 4; i++)
            optionsString += (options[i] + "\n");
        return  optionsString;
    }
    
    /*returns a concatenated String consisting of the question, possible answers, 
     and letter choice of the answer*/
    public String toString(){

        getOptionsString();

        return question + "\n" + optionsString + answer;
    }
    
    //returns the letter choice and the answer of the question
    public String[] getCorrectOption()
    {
    	String[] answers = new String[2];
    	for(int i = 0; i < options.length; i++)
    	{
    		if(options[i].substring(0, 1).equals(answer))
    		{
    			answers[0] = question;
    			answers[1] = options[i];
    			return answers;
    		}
    	}
    	return null;
    }

}
