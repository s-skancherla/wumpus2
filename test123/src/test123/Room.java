package test123;

public class Room {

	boolean triviaTold;
	String[] triviaAnswer;
	int number;
	int xCoordinate;
	int yCoordinate;
	int [] connectedRooms;
	int roomNumber;
	
	//Room constructor
	public Room(int num, int x, int y, int roomNum, String[] hint) {
		number = num;
		xCoordinate = x;
		yCoordinate = y;
		roomNumber = roomNum;
		connectedRooms = new int [4];
		triviaTold = false;
		this.triviaAnswer = hint;
	}
	
	//sets the connections to this room
	public void setConnections(int[] connectedRoom) {
		for(int i = 0; i < connectedRoom.length; i++)
		{
			this.connectedRooms[i] = connectedRoom[i];
		}
	}
	
	//returns a String array of length 2 with the question and answer to the question
	public String[] getTriviaAnswer()
	{
		return triviaAnswer;
	}
	
	//returns true if the hint was already given, false otherwise
	public boolean getTriviaTold()
	{
		return triviaTold;
	}
	
	//sets the parameter newTrivia to triviaTold
	public void setTriviaTold(boolean newTrivia)
	{
		triviaTold = newTrivia;
	}
	
	//returns the index of the roomNumber
	public int getNumber() {
		return number;
	}
	
	//returns the x-coordinate of the room (row index of the room)
	public int getX() {
		return xCoordinate;
	}
	
	//returns the y-coordinate of the room (column index of the room)
	public int getY() {
		return yCoordinate;
	}
	
	//returns the roomNumber
	public int getRoomNum() {
		return roomNumber;
	}
	
	//returns an array of connections
	public int[] getConnection() {
		return connectedRooms;
	}
	
	//returns a concatenated String with the roomNumber, x-coordinate, and y-coordinate
	public String toString() {
		return "Room number: " + number + ", X Coordinate: " + xCoordinate + ", Y Coordinate: " + yCoordinate;
	}
}
