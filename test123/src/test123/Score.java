package test123;

import java.io.Serializable;
public class Score implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int score;
    private int caveNum;
    private String playerName;
    
    //Score constructor
    public Score(int score, String playerName, int caveNum)
    {
        this.score = score;
        this.playerName = playerName;
        this.caveNum = caveNum;
    }
    
    //returns the score of the player
    public int getScore(){
        return score;
    }
    
    //returns the player's name
    public String getPlayerName(){
        return playerName;
    }
    
    //returns the number of the cave that was played
    public int getCaveName(){
        return caveNum;
    }
    
    //returns a concatenated String with the player's score, name, and cave that was played
    public String toString(){
        return "Player Name: " + playerName + ", Score: " + score + ", Cave Name: " + caveNum + "\n";
    }
}
