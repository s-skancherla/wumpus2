package test123;

import java.util.Comparator;
public class ScoreComparator implements Comparator<Score>{
	/*compares two different score objects and returns -1 if the firstScore is greater than
	the secondScore, 1 if the firstScore is less than the secondScore, and 0 if
	they're the same*/
	public int compare(Score score1, Score score2)
	{
		int firstScore = score1.getScore();
		int secondScore = score2.getScore();
		if(firstScore < secondScore)
		{
			return 1;
		}
		else if(secondScore < firstScore)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
}

