package test123;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class Trivia {

	//number of questions in question bank
	private static final int NUM_TOTAL_QUESTIONS = 129;

    private static Question[] questions;
    private static boolean[] hasBeenAsked;
    private static boolean[] givenHint;
    private Question currentQuestion;
    private Scanner input;
    //Trivia Constructor
    public Trivia() throws IOException{

        boolean isMac = false;

        if (isMac) {
            input = new Scanner(new File("test123/input/triviaQuestions.txt"));
        } else {
            input = new Scanner(new File ("./input/triviaQuestions.txt"));
        }

        //input = new Scanner(new File(path + "triviaQuestions.txt"));
        hasBeenAsked = new boolean[NUM_TOTAL_QUESTIONS];
        givenHint = new boolean[NUM_TOTAL_QUESTIONS];
        questions = readFile();

    }
    //returns an array of questions by reading a .txt file
    private Question[] readFile(){

        Question[] readingQuestions =  new Question[NUM_TOTAL_QUESTIONS];
        String question;
        String[][] options = new String[NUM_TOTAL_QUESTIONS][4];
        String answer;

        for(int i = 0; i < NUM_TOTAL_QUESTIONS; i++){

            question = input.nextLine();
            for(int j = 0; j < 4; j++)
                options[i][j] = input.nextLine();
            answer = input.nextLine();
            readingQuestions[i]= new Question(question, options[i], answer);

        }
        return readingQuestions;
    }
    //returns an ArrayList of questions that are going to be asked
    public static ArrayList<Question> getQuestion(int numQuestions){
        ArrayList<Question> questionHolder = new ArrayList<Question>();
        int num = -1;
        for(int i = 0; i < numQuestions; i++){
            num = (int)(Math.random()*NUM_TOTAL_QUESTIONS);
            if(hasBeenAsked[num]){
                i--;
            }
            else{
                questionHolder.add(questions[num]);
                hasBeenAsked[num] = true;
            }
        }
        return questionHolder;
    }
    //returns a concatenated String which includes the question and possible answers
    public String printCurrentQuestion() {
    	return currentQuestion.getQuestion() + "\n" + currentQuestion.getOptionsString();
    }
    //returns true if the user chose the correctAnswer and false otherwise
    public boolean checkAnswer(String answer) {
    	if(answer.equals(currentQuestion.getAnswer())) {
    		return true;
    	}
    	return false;
    	
    }
    //sets the currentQuestion to a new question
    public void setCurrentQuestion(Question question) {
    	currentQuestion = question;
    }
    //gets a hint for the user when the user goes through a tunnel
    public String[] getHint()
    {
    	String[] answer = new String[2];
    	boolean hintFound = false;
    	while(hintFound == false)
    	{
    		int j = (int)(Math.random() * 120 + 1);
    		System.out.println(j);
    		if(hasBeenAsked[j] == false && givenHint[j] == false)
    		{
    			answer = questions[j].getCorrectOption();
    			givenHint[j] = true;
    			hintFound = true;
    		}
    	}
    	return answer;
    }
}
