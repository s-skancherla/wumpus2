package test123;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class TriviaPrinter extends JFrame implements KeyListener{

	
	//Fields
	private int mode;
	private int maxCorrect;
	private int maxQuestions;
	private int numQuestions;
	private Trivia trivia;
	private boolean result;
	private String secret;
	private Player player;
	
	
	public void paint(Graphics g) {
		
		//Sets background
		g.setColor(Color.WHITE);
		g.fillRect(0,0,800,480);
		
		//Depending on type of trivia game, sets number of questions needed to get correct and how many questions to display
		if(mode == 1 || mode == 2 || mode == 3) {
			maxCorrect = 2;	
			maxQuestions = 3;
		}
		
		if(mode == 4) {
			maxCorrect = 3;
			maxQuestions = 5;
		}
		
		//Gets questions from trivia
		ArrayList<Question> questionHopper = trivia.getQuestion(numQuestions);
		
		//Variables that will count how many questions the user has gotten correct and what questions the user is on
		int correct = 0;
		int qOn = 0;
		
		//Primary loop for playing the trivia round
		while(correct < maxCorrect && qOn < maxQuestions && player.getGold() > 0) {
			
			//Subtracts player gold by one for playing trivia
			player.changeGold(-1);
			
			//Clears the screen effectively
			g.setColor(Color.WHITE);
			g.fillRect(0,0,800,480);
			
			//Initializes variables for arranging text
			int yTrivia = 100;
			Dimension d = this.getSize();
			g.setColor(Color.BLACK);
			g.setFont(new Font("Helvetica", Font.BOLD, 20));
			
			//Depending on purpose of trivia, prints certain message
			if(mode == 1) {
				drawCenteredString("YOU HAVE CHOSEN TO PURCHASE AN ARROW", d.width, yTrivia, g);
				yTrivia += 30;
				drawCenteredString("ANSWER 2 OF 3 TRIVIA QUESTIONS TO EARN AN ARROW!", d.width, yTrivia, g);
				maxCorrect = 2;
			}
			
			else if(mode == 2) {
				drawCenteredString("YOU HAVE CHOSEN TO PURCHASE A SECRET", d.width, yTrivia, g);
				yTrivia += 30;
				drawCenteredString("ANSWER 2 OF 3 TRIVIA QUESTIONS TO EARN A SECRET!", d.width, yTrivia, g);
				maxCorrect = 2;
			}
			
			
			else if(mode == 3) {
				drawCenteredString("YOU ENTERED A ROOM WITH A PIT!", d.width, yTrivia, g);
				yTrivia += 30;
				drawCenteredString("ANSWER 2 OF 3 TRIVIA QUESTIONS CORRECTLY TO SURVIVE!", d.width, yTrivia, g);
				maxCorrect = 2;
			}
			else {
				drawCenteredString("YOU HAVE ENCOUNTERED THE WUMPUS", d.width, yTrivia, g);
				yTrivia += 30;
				drawCenteredString("ANSWER 3 OF 5 TRIVIA QUESTIONS CORRECTLY TO SURVIVE!", d.width, yTrivia, g);
				maxCorrect = 3;
			}
			yTrivia += 50;
			
			//Prints the question with the proper format
			trivia.setCurrentQuestion(questionHopper.get(qOn));
			
			//Gets the question
			String toAsk = questionHopper.get(qOn).getQuestion();
			
			//The following code properly arranges the question in the middle of the screen
			String savedQ = toAsk;
			boolean printQuestion = true;
			int onIndex = 0;
			String toPrint = "";
			while(printQuestion) {
				int lengthRem = toAsk.length();
				if(lengthRem > 40) {
					toPrint = toAsk.substring(0,40);
					toAsk = toAsk.substring(40);
					onIndex += 40;
					while(onIndex < savedQ.length() && toAsk.charAt(0) != ' ') {
						toPrint += toAsk.charAt(0);
						toAsk = toAsk.substring(1);
						onIndex++;
					}
					drawCenteredString(toPrint,d.width,yTrivia,g);
					yTrivia+=20;
				}
				else {
					drawCenteredString(toAsk,d.width,yTrivia,g);
					printQuestion = false;
				}
			}
			//End

			yTrivia += 30;
			
			//Prints the possible options
			String[]options = questionHopper.get(qOn).getOptions();
			for(int j = 1; j <= 4; j++) {
				drawCenteredString(options[j-1],d.width,yTrivia,g);
				yTrivia+=20;
			}
			yTrivia+=30;
			
				
			
			//Opens a JOptionPanel where user can input their answer
			JOptionPane answerChoice = new JOptionPane();
            String answer = null;
            while (answer == null) {
                answer = answerChoice.showInputDialog(null, "Enter your answer: ");
            }
            
            
			//Dialog box opens displaying whether the user answered correctly or not
            yTrivia += 30;
            qOn++;
            g.setFont(new Font("Helvetica", Font.BOLD, 15));
            if(trivia.checkAnswer(answer)) {
            	correct++;
                JOptionPane.showMessageDialog(null, "That answer is correct!\nYou are " + correct + " for " + qOn);
            }
            else {
            	JOptionPane.showMessageDialog(null, "That answer is incorrect!\nYou are " + correct + " for " + qOn);
            }
            
            //Resets yTrivia
            yTrivia = 100;
            
		}  

		//Goes to the endgame screen if the user doesnt have enough gold to continue
		if(player.getGold() == 0) {
			EndgamePrinter test = new EndgamePrinter(5, player);
		}
		
		//Otherwise show another message
		else{
			if(correct == maxCorrect) {
				result = true;
			}
			drawResultsScreen(g);
		}	
	}
	
	//This method allows text to be centered in the screen
	public void drawCenteredString(String s, int w, int h, Graphics g) {
		FontMetrics fm = g.getFontMetrics();
		int x = (w - fm.stringWidth(s)) / 2;
		g.drawString(s,x,h);
	}
	
	//Prints the results screen and potentially ends the game
	public void drawResultsScreen(Graphics g) {
		
		//Resets the screen
		g.setColor(Color.WHITE);
		g.fillRect(0,0,800,480);
		
		//Initializes variables for formatting
		int yResult = 100;
		Dimension d = this.getSize();
		g.setColor(Color.BLACK);
		g.setFont(new Font("Helvetica", Font.BOLD, 20));
		
		//Depending on the purpose of the trivia game, a certain message is displayed and appropriate actions are taken
		//For purchasing arrows
		if(mode == 1) {
			if(result) {
				drawCenteredString("YOU PASSED!", d.width, yResult, g);
				yResult += 40;
				g.setFont(new Font("Helvetica", Font.BOLD, 25));
				drawCenteredString("You earned one arrow!", d.width, yResult, g);
				player.gainArrow();
			}
			else {
				drawCenteredString("YOU FAILED...", d.width, yResult, g);
				yResult += 30;
				g.setFont(new Font("Helvetica", Font.BOLD, 15));
				drawCenteredString("Try to earn an arrow again later!", d.width, yResult, g);
			}
		}
		
		//For purchasing secrets
		if(mode == 2) {
			if(result) {
				drawCenteredString("YOU PASSED!", d.width, yResult, g);
				yResult += 30;
				g.setFont(new Font("Helvetica", Font.BOLD, 15));
				drawCenteredString("You earned one secret!", d.width, yResult, g);
				yResult+=60;
				g.setFont(new Font("Helvetica", Font.BOLD, 25));
				drawCenteredString(secret, d.width, yResult, g);
			}
			else {
				drawCenteredString("YOU FAILED...", d.width, yResult, g);
				yResult += 30;
				g.setFont(new Font("Helvetica", Font.ITALIC, 15));
				drawCenteredString("Try to earn a secret again later!", d.width, yResult, g);
			}
		}
		
		//For a pit
		if(mode == 3) {
			if(result) {
				drawCenteredString("YOU PASSED!", d.width, yResult, g);
				yResult += 30;
				g.setFont(new Font("Helvetica", Font.BOLD, 15));
				drawCenteredString("YOU SAVED YOURSELF FROM A CRUSHING DEFEAT!", d.width, yResult, g);
				yResult+=60;
			}
			else {	
				EndgamePrinter test2 = new EndgamePrinter(2, player);
			}	
		}
		
		//For a wumpus encounter
		if(mode == 4) {
			if(result) {
				drawCenteredString("YOU PASSED!", d.width, yResult, g);
				yResult += 30;
				g.setFont(new Font("Helvetica", Font.BOLD, 15));
				drawCenteredString("YOU SAVED YOURSELF FROM BEING EATEN BY THE WUMPUS!", d.width, yResult, g);
				yResult += 30;
				drawCenteredString("THE WUMPUS RAN A COUPLE ROOMS AWAY...", d.width, yResult, g);
				yResult += 30;
				drawCenteredString("WATCH OUT!", d.width, yResult, g);
				yResult+=60;
			}
			else {
				
				EndgamePrinter test2 = new EndgamePrinter(1, player);
			}	
		}
		yResult += 175;
		
		//Notifies user about how to return to the game
		g.setFont(new Font("Helvetica", Font.ITALIC, 25));
		drawCenteredString("CLICK 'ESCAPE' TO RETURN TO THE GAME!", d.width, yResult, g);
		
		
	}
	
	//Constructor
	public TriviaPrinter(int mode, int numQuestions, Player player) throws IOException {
		setSize(800, 480);
        setResizable(false);
        setTitle("Main Menu");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBackground(Color.black);
        setLocationRelativeTo(null);
        setVisible(true);
        this.mode = mode;
        trivia = new Trivia();
        this.numQuestions = numQuestions;
        result = false;
        addKeyListener(this);
        secret = "";
        this.player = player;
	}
	
	//Returns the result of the trivia game
	public boolean getResult() {
		return result;
	}
	
	//Sets the secret for the user
	public void setSecret(String toAdd) {
		secret = toAdd;
	}

	//Listens for key clicks
	@Override
	public void keyPressed(KeyEvent e) {
		
		//Returns to game
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			dispose();
    	}
	}

	@Override
	public void keyReleased(KeyEvent e) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}
}
